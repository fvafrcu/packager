  devtools::check(vignettes = FALSE)
- check all testing stuff via: 
  $ grep "test_" inst/runit_tests/* | grep "^test_" -v
- condition the vignette building on existance.
- add checkmate to all exported functions
FIX ERROR FOR NO INTERNET?!
= new features
- add revdepcheck to make chain as prereq to cran_comments, incorporate into
  cran_comments.md
= old
- usethis's testthat.R-template misses " in library(), fix that somehow?
- work on  log/usage.log
- get rid of file.path() with relative paths
  in provide\_cran\_comments:
        check <- parse_check_results(file.path(path, check_log))
  and maybe elsewhere
- document the options (force)
- enhance coverage
- fix todo in git\_add\_commit() (git2r::status)
- try to re-implement use digest::sha1() for unit testing.
= standard 
- check on logs

- Tag commit ff21c2c230fee3c85da834530c95221aa7314397 as 1.15.2, once package is on CRAN using
	git tag -a 1.15.2 ff21c2c230fee3c85da834530c95221aa7314397 -m 'CRAN release'
  and checkout the developement version using packager::use_dev_version() or
	make use_dev_version
- Tag commit a2913cd71c026d3b7d13cd224470a6fc7c685889 as 1.15.2, once package is on CRAN using
	git tag -a 1.15.2 a2913cd71c026d3b7d13cd224470a6fc7c685889 -m 'CRAN release'
  and checkout the developement version using packager::use_dev_version() or
	make use_dev_version
