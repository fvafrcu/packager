R Under development (unstable) (2018-07-01 r74949)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Debian GNU/Linux 9 (stretch)

Matrix products: default
BLAS: /home/nik/svn/R/r-devel/build/lib/libRblas.so
LAPACK: /home/nik/svn/R/r-devel/build/lib/libRlapack.so

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] tools     stats     graphics  grDevices utils     datasets  methods  
[8] base     

other attached packages:
 [1] rcmdcheck_1.3.2  pkgbuild_1.0.3   pkgload_1.0.2    remotes_2.0.2   
 [5] checkmate_1.9.1  fakemake_1.4.1   httr_1.4.0       cyclocomp_1.1.0 
 [9] whoami_1.3.0     withr_2.1.2      desc_1.2.0       whisker_0.3-2   
[13] git2r_0.25.2     crayon_1.3.4     usethis_1.4.0    devtools_2.0.1  
[17] codetools_0.2-15 rprojroot_1.3-2 

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.1        magrittr_1.5      R6_2.4.0          rlang_0.3.3      
 [5] xopen_1.0.0       sessioninfo_1.1.1 cli_1.1.0         assertthat_0.2.1 
 [9] digest_0.6.18     processx_3.3.0    callr_3.2.0       fs_1.2.7         
[13] ps_1.3.0          testthat_2.0.0    memoise_1.1.0     glue_1.3.1       
[17] compiler_3.6.0    backports_1.1.3   prettyunits_1.0.2 jsonlite_1.6     
