install.packages("RWsearch")
library(RWsearch)
crandb_down()   # 20"

## GRAPHS
## GRAPHS
p_graphF(packager, recursive = TRUE)
ep <- c("graphics", "grDevices", "methods", "stats", "tools", "utils",
"devtools", "usethis")
p_graphF(packager, recursive = TRUE, exclpkgs = ep)

## DETAILS
p_deps(packager, recursive = TRUE) -> pp ; pp
p_deps(devtools, usethis, recursive = TRUE) -> pdu ; pdu
'%nin%' <- function (x, y) x[match(x, y, nomatch = 0) == 0]
unique(unlist(pp)) %nin% unique(unlist(pdu))

crandb_load()
l <- list(
p_deps(HandTill2001, recursive = TRUE),
p_deps(maSAE, recursive = TRUE),
p_deps(packager, recursive = TRUE),
p_deps(fakemake, recursive = TRUE),
p_deps(document, recursive = TRUE),
p_deps(rasciidoc, recursive = TRUE),
p_deps(excerptr, recursive = TRUE),
p_deps(cleanr, recursive = TRUE)
)
sort(sapply((sapply(l, "[")), length))
