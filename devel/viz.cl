Rscript -e 'pkgload::load_all();pkgload::load_all("~/packager");  ml <- packager::get_package_makelist(); fakemake::write_makefile(ml, "/tmp/tmp_makelist")'
sed -e 's#R/.*\.R\>##g' -e 's#inst/.*\.R\>##g'   -e 's#man/.*\.Rd\>##g' < /tmp/tmp_makelist > /tmp/makelist
make -Bnd -f /tmp/makelist log/cran_comments.Rout | make2graph | dot -Tpng -o /tmp/makelist.png
eog /tmp/makelist.png
sed -e 's#^\(.*_FILES\) = .*#\1 : \1#g' < Makefile  > /tmp/Makefile
make -Bnd -f /tmp/Makefile cran-comments.md | make2graph | dot -Tpng -o /tmp/makefile.png
eog /tmp/makefile.png

