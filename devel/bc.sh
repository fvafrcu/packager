R-devel --vanilla CMD build --no-build-vignettes "."
package=$(grep "^Package:" DESCRIPTION | cut -f2 -d' ')
version=$(grep "^Version:" DESCRIPTION | cut -f2 -d' ')
R-devel --vanilla CMD check --no-build-vignettes ${package}_${version}.tar.gz
