Dear CRAN Team,
this is a resubmission of package 'packager'. I have added the following changes:

- Responding to  Sun, 9 Feb 2020, I have removed the call to `sudo`.
- Responding to Uwe Ligges' request Wed, 8 Jan 2020, I have fixed spelling in
  file DESCRIPTION.
- Responding to Martina Schmirl's request on Wed, 8 Jan 2020, I have updated the
  year in file LICENSE.
- According to Swetlana Herbrandt's request on Sun, 15 Dec 2019, I have 
    * fixed fields `Title` and `Description` of file DESCRIPTION,
    * removed all calls to package cyclocomp from the examples and tests to ensure
      I do not install (new versions of already installed) packages.
- According to Jelena Saf's request on Fri, Dec 20, 2019, I have
    * fixed fields `Description` of file DESCRIPTION,
    * skipped tests on CRAN.

Best, Andreas Dominik

# Package packager 1.0.0

Reporting is done by packager version 1.0.0

## Test  environments 
- R Under development (unstable) (2020-01-30 r77751)
    Platform: x86_64-pc-linux-gnu (64-bit)
    Running under: Devuan GNU/Linux ascii
    0 errors | 0 warnings | 1 note 
- gitlab.com
  R version 3.6.2 (2019-12-12)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Debian GNU/Linux 10 (buster)
  Status: 1 WARNING, 1 NOTE
- win-builder (devel)

## Local test results
- RUnit:
    packager_unit_test - 17 test functions, 0 errors, 0 failures
- Testthat:
    OK: 2, Failed: 0, Warnings: 3, Skipped: 0
- Coverage by covr:
    packager Coverage: 52.08%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for add_github_url_to_desc by 2.
     Exceeding maximum cyclomatic complexity of 10 for release by 2.
     Exceeding maximum cyclomatic complexity of 10 for submit by 2.
- lintr:
    found 147 lints in 2946 lines of code (a ratio of 0.0499).
- cleanr:
    found 43 dreadful things about your code.
- codetools::checkUsagePackage:
    found 60 issues.
- devtools::spell_check:
    found 50 unkown words.
