logs=$(find log/ -name "*.log" -print)
for log in  $logs
do
    rout=${log%.log}.Rout
    if [ -e $rout ]; then
        ll=$(wc -l $log  | cut -f1 -d' ')
        lr=$(wc -l $rout | cut -f1 -d' ')
        if [ $ll -ne $lr ]; then 
            diffuse $log  $rout
        fi
    else 
        echo $rout does not exist!
    fi 
done

