root <- file.path("~", "git", "cyclops", "fvafrcu")

my_packages <- c("HandTill2001", "maSAE", "document", "excerptr",
                 "cleanr", "rasciidoc", "fakemake", "packager")
packages <- c(my_packages, "cptables", "cuutils", "wehamr", "rbdatpro", "regulaFalsi", "treeHeightImputation")

for (path in file.path(root, packages)) {
    packager:::sanitize_usethis_git_hook(path)
    packager::use_git_check_version_not_tagged(path)
}
