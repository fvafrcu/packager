pkgload::load_all(".")
build_manual()
fritools::view(fritools::file_modified_last(path = ".", pattern = "\\.pdf$",
                                            recursive = TRUE), "evince")

fritools::find_missing_see_also(".")
fritools::find_missing_family(".")
fritools::delete_trailing_whitespace(path = "R", pattern = "\\.[rR]$")
fritools::delete_trailing_whitespace(path = file.path("inst", "runit_tests"),
                                     pattern = "\\.[rR]$")
fritools::delete_trailing_whitespace(path = "inst", recursive = TRUE,
                                     pattern = "\\.[rR]$")
########### 




fritools:::r_cmd_install(path = fritools:::r_cmd_build("."))

fritools::get_package_version("fritools")
fritools::get_package_version("packager")
compare_logs(path = ".")
compare_logs(path = ".", call_diff = TRUE)
print(c <- compare_make())
identical(c[,1], c[,2])

# run a target only
target_name <- "tinytest"
if (devtools::as.package(".")[["package"]] == "packager") pkgload::load_all()
makelist <- packager::get_package_makelist()
print(fakemake::make(target_name, makelist, force = TRUE, recursive = FALSE))

# tinytest
create("/tmp/foo", fakemake = FALSE)
path <- "~/fritools"
res <- check_rhub(path, os = "m1")
