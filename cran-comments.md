Dear CRAN Team,
this is a resubmission of package 'packager'. I have added the following changes:

* Fixed CRAN notes on Escaped LaTeX specials.
* Fix `provide_news_rd()` to deal with escaped underscores.

Please upload to CRAN.
Best, Andreas Dominik

# Package packager 1.15.2

Reporting is done by packager version 1.15.2


## Test environments
- R Under development (unstable) (2023-07-29 r84787)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 4 (chimaera)
   0 errors | 0 warnings | 1 note 
- win-builder (devel)  
  Date: Wed, 16 Aug 2023 13:47:09 +0200
  Your package packager_1.15.2.tar.gz has been built (if working) and checked for Windows.
  Please check the log files and (if working) the binary package at:
  https://win-builder.r-project.org/k487w54CMMLt
  The files will be removed after roughly 72 hours.
  Installation time in seconds: 10
  Check time in seconds: 415
  Status: OK
  R version 4.3.1 (2023-06-16 ucrt)
   

## Local test results
- RUnit:
    packager_unit_test - 20 test functions, 0 errors, 0 failures in 63 checks.
- testthat:
    [ FAIL 0 | WARN 1 | SKIP 0 | PASS 2 ]
- tinytest:
    All ok, 1 results (56ms)
- Coverage by covr:
    packager Coverage: 52.40%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for compare_logs by 9.
     Exceeding maximum cyclomatic complexity of 10 for use_dependency by 8.
     Exceeding maximum cyclomatic complexity of 10 for release by 5.
     Exceeding maximum cyclomatic complexity of 10 for submit by 5.
     Exceeding maximum cyclomatic complexity of 10 for install_deps by 3.
     Exceeding maximum cyclomatic complexity of 10 for add_github_url_to_desc by 2.
     Exceeding maximum cyclomatic complexity of 10 for check_rhub by 1.
- lintr:
    found 915 lints in 3820 lines of code (a ratio of 0.2395).
- cleanr:
    found 35 dreadful things about your code.
- codetools::checkUsagePackage:
    found 64 issues.
- devtools::spell_check:
    found 76 unkown words.
